# A collection of tutorials teaching C++ #

The university I attended takes the following steps to teach programming:

1. Teach you java
2. Teach you linear data structures using Java
3. Teach you non-linear data structures using C++ **WITHOUT TEACHING YOU C++!!!**

This was no problem for me, because I had taught myself C++ for fun when I was in high school.
However, most of my peers were not so lucky.  And now, nearly 10 years later, this is still the
way this university teaches.  So decided to write these tutorials to help my future co-workers
become passable C++ programmers.

This tutorial uses C++14.  Unlike C# and Java, there is no reference C++ compiler, but there is
a plethora of compiler toolchains:

 * **Microsoft** It's been a while since I've used a Microsoft compiler
 * **GNU GCC** (GNU Compiler Collection) best option if you have GNU/Linux
 * **Cygwin** (The Windows port of the GNU GCC) best option if you want a program written in the POSIX API to work on Windows
 * **MinGW** (Minimalist GNU for Windows) best option if you want to write a Windows program on Windows without paying money
 * **LLVM** I don't know much about this one

There are also many IDEs.  However, because of the lack of good IDEs (C++ is notoriously hard to parse) many programmers just
use Vim or Emacs.  Below are some of the IDEs that I know of:

 * Commercial
    * [Jetbrains CLion](https://www.jetbrains.com/clion/) I'm a Jetbrains fanboy, so this is the IDE I'm using.
    * [Visual Studio](https://www.visualstudio.com/)/[Visual C++](https://www.visualstudio.com/vs/cplusplus/)
    * [C++ Builder](https://www.embarcadero.com/products/cbuilder) I haven't used C++ Builder since the early 2000's.  Since
      then the original creator (Borland) was bought out by Embarcadero.  Back in the 2000's this was the best C++ IDE on the
      market, but I can't vouge for or against the quality of this product since then.
 * Open Source
    * [Eclipse CDT](https://eclipse.org/cdt/)
    * [Code::Blocks](http://www.codeblocks.org/)
    * [Geany](https://www.geany.org/)

This tutorial uses the [CMake](http://www.cmake.org/) build tool.

## How to use this tutorial

If you want to learn all of C++ then start with the oldest commit and work your way up. If you only want to learn a certain
aspect about C++ then search the tags for the aspect you are looking for.

There are also many great online references, such as:

 * [cppreference.com](http://cppreference.com)
 * www.cplusplus.com

However, these sites are designed to be references for people who already know C++.