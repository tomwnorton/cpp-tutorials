#include <iostream>
#include "Box.hpp"
#include "average.hpp"


int main() {
    int numbers[] = {9, 7, 12, 14, 19};
    std::cout << calculateAverage(numbers, sizeof(numbers) / sizeof(int)) << std::endl;

    Box box = {10, 20, 30};
    displayBox(box);

    typedef void (*FunctionPointer)(const Box&);
    FunctionPointer functionPointer = displayBox;
    functionPointer(box);
}