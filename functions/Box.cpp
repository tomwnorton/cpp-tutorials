#include <iostream>
#include "Box.hpp"

// It's best practice to pass large structs and classes as references
// If you aren't going to change the struct or class instance, then use a constant reference
void displayBox(const Box &box) {
    std::cout << "length: " << box.length << " "
              << "width: " << box.width << " "
              << "height: " << box.height
              << std::endl;
}