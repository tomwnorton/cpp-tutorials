int calculateAverage(int numbers[], int numbersLength) {
    int average = 0;
    for (int *pos = numbers; pos != numbers + numbersLength; ++pos) {
        average += *pos;
    }
    return average;
}