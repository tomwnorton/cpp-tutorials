// These lines starting with # are preprocessor directives like #include
// This C++ idiom is called a header guard and ensures that a header file is never included into a source file more than
// once.
#ifndef TUTORIAL_BOX_HPP
#define TUTORIAL_BOX_HPP

struct Box {
    int length;
    int width;
    int height;
};

void displayBox(const Box &box);

#endif //TUTORIAL_BOX_HPP
