// These lines starting with # are preprocessor directives like #include
// This C++ idiom is called a header guard and ensures that a header file is never included into a source file more than
// once.
#ifndef TUTORIAL_AVERAGE_HPP
#define TUTORIAL_AVERAGE_HPP

int calculateAverage(int numbers[], int numbersLength);

#endif //TUTORIAL_AVERAGE_HPP
