#include <iostream> // Lets us use cout, cin, and endl
#include <iomanip>  // Lets us use setw and setfill
#include <limits>   // Lets us use the numeric_limits class
#include <string>   // Lets us use the string class

const int TYPE_WIDTH = 16;
const int SIZE_WIDTH = 4;
const int MIN_WIDTH = 24;
const int MAX_WIDTH = 24;

int main() {
    // These metrics may vary depending on architecture:
    std::cout << std::setw(TYPE_WIDTH) << "Type" << "|"
              << std::setw(SIZE_WIDTH) << "Size" << "|"
              << std::setw(MIN_WIDTH) << "Min" << "|"
              << std::setw(MAX_WIDTH) << "Max"
              << std::endl

              << std::setfill('-')
              << std::setw(TYPE_WIDTH) << "" << "|"
              << std::setw(SIZE_WIDTH) << "" << "|"
              << std::setw(MIN_WIDTH) << "" << "|"
              << std::setw(MAX_WIDTH) << ""
              << std::endl
              << std::setfill(' ')

              << std::setw(TYPE_WIDTH) << "short" << "|"
              << std::setw(SIZE_WIDTH) << sizeof(short) << "|"
              << std::setw(MIN_WIDTH) << std::numeric_limits<short>::min() << "|"
              << std::setw(MAX_WIDTH) << std::numeric_limits<short>::max()
              << std::endl

              << std::setw(TYPE_WIDTH) << "int" << "|"
              << std::setw(SIZE_WIDTH) << sizeof(int) << "|"
              << std::setw(MIN_WIDTH) << std::numeric_limits<int>::min() << "|"
              << std::setw(MAX_WIDTH) << std::numeric_limits<int>::max()
              << std::endl

              << std::setw(TYPE_WIDTH) << "long" << "|"
              << std::setw(SIZE_WIDTH) << sizeof(long) << "|"
              << std::setw(MIN_WIDTH) << std::numeric_limits<long>::min() << "|"
              << std::setw(MAX_WIDTH) << std::numeric_limits<long>::max()
              << std::endl

              << std::setw(TYPE_WIDTH) << "long long" << "|"
              << std::setw(SIZE_WIDTH) << sizeof(long long) << "|"
              << std::setw(MIN_WIDTH) << std::numeric_limits<long long>::min() << "|"
              << std::setw(MAX_WIDTH) << std::numeric_limits<long long>::max()
              << std::endl

              << std::setw(TYPE_WIDTH) << "char" << "|"
              << std::setw(SIZE_WIDTH) << sizeof(char) << "|"
              << std::setw(MIN_WIDTH) << std::numeric_limits<char>::min() << "|"
              << std::setw(MAX_WIDTH) << std::numeric_limits<char>::max()
              << std::endl

              << std::setw(TYPE_WIDTH) << "float" << "|"
              << std::setw(SIZE_WIDTH) << sizeof(float) << "|"
              << std::setw(MIN_WIDTH) << std::numeric_limits<float>::min() << "|"
              << std::setw(MAX_WIDTH) << std::numeric_limits<float>::max()
              << std::endl

              << std::setw(TYPE_WIDTH) << "double" << "|"
              << std::setw(SIZE_WIDTH) << sizeof(double) << "|"
              << std::setw(MIN_WIDTH) << std::numeric_limits<double>::min() << "|"
              << std::setw(MAX_WIDTH) << std::numeric_limits<double>::max()
              << std::endl

              << std::setw(TYPE_WIDTH) << "long double" << "|"
              << std::setw(SIZE_WIDTH) << sizeof(long double) << "|"
              << std::setw(MIN_WIDTH) << std::numeric_limits<long double>::min() << "|"
              << std::setw(MAX_WIDTH) << std::numeric_limits<long double>::max()
              << std::endl;

    int firstInteger;
    int secondInteger;
    std::cout << "Enter any two integers separated by whitespace: ";
    std::cin >> firstInteger >> secondInteger;
    std::cout << "You entered " << firstInteger << " and " << secondInteger << std::endl;

    /* Any integer type can be unsigned (char is considered an integer type).  Try typing a negative number at the
     * prompt.  Notice how it doesn't explode but instead interprets the binary data as a positive number.
     * The computer represents a 4-byte integer with value 56 as  0x00 00 00 38.
     * The computer represents a 4-byte integer with value -56 as 0xFF FF FF C8.
     */
    unsigned int thirdInteger;
    std::cout << "Enter a third integer: ";
    std::cin >> thirdInteger;
    std::cout << "You entered ";
    std::cout.flags(std::ios::hex | std::ios::uppercase);
    std::cout << thirdInteger << std::endl;
    std::cout.flags(std::ios::dec);

    // In C and C++ variables are not automatically initialized
    int uninitialized;
    std::cout << "The untouched memory value: " << uninitialized << std::endl;

    // You can also look at the memory address of a variable with the address-of operator
    // NOTE: When used as a unary operator the & is the address-of operator.
    //       When used as a binary operator the & is as the bitwise-and operator.
    std::cout << "The memory address of uninitialized: " << &uninitialized << std::endl;

    // Objects can also be stack variables
    std::string hello = "hello";
    std::string world = "world";
    std::string helloWorld = hello + " " + world;
    std::cout << "The string \"" << helloWorld << "\" is " << helloWorld.length() << " characters long" << std::endl;

    return 0;
}