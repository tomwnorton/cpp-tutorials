#include <iostream>

struct Box {
    int length;
    int height;
    int width;
};

int main() {
    int firstStackVariable = 23;
    int secondStackVariable = 30;
    int* firstPointer = &firstStackVariable;

    *firstPointer = 102;
    std::cout << "firstStackVariable: " << firstStackVariable << std::endl;

    // You can reassign a pointer
    firstPointer = &secondStackVariable;
    *firstPointer = 987;
    std::cout << "secondStackVariable: " << secondStackVariable << std::endl;

    // You can also allocate memory off of the heap
    firstPointer = new int;
    *firstPointer = 10;
    std::cout << "heap value: " << *firstPointer << std::endl;
    // Always remember to delete anything you new up!
    delete firstPointer;

    // In C++, const variables are entire different types from non-const variables.
    const int MY_CONSTANT = 9876;
    const int OTHER_CONSTANT = 123456;
    const int* pointerToConstant = &MY_CONSTANT;
    pointerToConstant = &OTHER_CONSTANT;
    // This code will not compile: *pointerToConstant = 9;

    // You can make a constant pointer
    int* const constantPointer = &firstStackVariable;
    // This code would not compile: constantPointer = &secondStackVariable;

    // You can also make a constant pointer to a constant
    const int* const constantPointerToConstant = &MY_CONSTANT;

    // When you have a pointer to a union, struct, or class, always use the points to operator instead of the member
    // access operator.
    Box box;
    box.length = 22;
    box.width = 12;
    box.height = 10;

    Box* pBox = &box;
    pBox->width = 30;

    std::cout << "box.width: " << box.width << std::endl;

    return 0;
}