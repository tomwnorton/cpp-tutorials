#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>

using namespace std;

int calculateTotal(const vector<int>& numbers) {
    // accumulate will work on all containers, including arrays
    return accumulate(numbers.begin(), numbers.end(), 0);
}

void displayVectorTotal(const vector<int>& numbers) {
    bool isFirst = true;
    for (int number : numbers) {
        if (isFirst) {
            isFirst = false;
        } else {
            cout << " + ";
        }
        cout << number;
    }
    cout << " = " << calculateTotal(numbers) << endl;
}

void displayVectorInReverse(const vector<int>& numbers) {
    for (auto pos = numbers.rbegin(); pos != numbers.rend(); ++pos) {
        cout << *pos << " ";
    }
    cout << endl;
}

struct DivisionInfo {
    int dividend;
    int divisor;
    int quotient;
    int remainder;
};

int main() {
    vector<int> myVector = {10, 17, 22, 13, 8, 22};
    displayVectorTotal(myVector);
    displayVectorInReverse(myVector);

    myVector.push_back(32);
    displayVectorTotal(myVector);

    // Use a lambda to display the first element greater than 20
    auto greaterPos = find_if(myVector.begin(), myVector.end(), [](auto element) {
        return element > 20;
    });
    if (greaterPos != myVector.end()) {
        cout << "first element greater than 20: " << *greaterPos << endl;
    }

    // Remove the last element. end points to the position after the last element
    myVector.erase(myVector.end() - 1);
    displayVectorTotal(myVector);

    // Remove the first occurrence of 22
    auto pos = find(myVector.begin(), myVector.end(), 22);
    if (pos != myVector.end()) {
        cout << "Removing the first occurrence of " << *pos << endl;
        myVector.erase(pos);
        displayVectorTotal(myVector);
    }



    /* Remove the second, third, and fourth elements
     * begin points to the first element.
     * This overload of erase removes elements within the range of [second element, fifth element)
     */
    myVector.erase(myVector.begin() + 1, myVector.begin() + 4);
    displayVectorTotal(myVector);

    myVector.clear();
    displayVectorTotal(myVector);

    // Copy vector to an array
    myVector = { 22, 98, 37, 26, 34, 28 };
    int arrayCopy[6];
    copy(myVector.begin(), myVector.end(), arrayCopy);
    for (int number : arrayCopy) {
        cout << number << " ";
    }
    cout << endl;

    /* Copy to another vector that is pre-allocated
     * copy doesn't know if the destination is an array or an STL container, so it has no way of adding elements to
     * the destination container.  So we need to pre-allocate the vector with the number of elements we need.
     */
    vector<int> firstVectorCopy(6);
    copy(myVector.begin(), myVector.end(), firstVectorCopy.begin());
    for (int number : firstVectorCopy) {
        cout << number << " ";
    }
    cout << endl;

    // We can use a special type of iterator called a back_inserter to automatically insert elements into the vector
    vector<int> secondVectorCopy;
    copy(myVector.begin(), myVector.end(), back_inserter(secondVectorCopy));
    for (int number : secondVectorCopy) {
        cout << number << " ";
    }
    cout << endl;

    // We can also copy only elements that meet some criteria
    vector<int> thirdVectorCopy;
    copy_if(myVector.begin(), myVector.end(), back_inserter(thirdVectorCopy), [](auto element) {
        return element > 30;
    });
    for (int number : thirdVectorCopy) {
        cout << number << " ";
    }
    cout << endl;

    // You can also transform the elements in a vector
    vector<DivisionInfo> divisionInfos;
    transform(myVector.begin(), myVector.end(), back_inserter(divisionInfos), [](auto element) {
        DivisionInfo divisionInfo;
        divisionInfo.dividend = element;
        divisionInfo.divisor = 5;
        divisionInfo.quotient = element / 5;
        divisionInfo.remainder = element % 5;
        return divisionInfo;
    });
    cout << endl;
    for (auto divisionInfo : divisionInfos) {
        cout << divisionInfo.dividend << " / " << divisionInfo.divisor << " = "
                  << divisionInfo.quotient << " r " << divisionInfo.remainder << endl;
    }
    cout << endl;

    return 0;
}