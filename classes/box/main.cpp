#include <iostream>
#include "Box.hpp"

using namespace std;

// This is the right way to pass an object to a function
void displayBox(const Box& box) {
    cout << "length: " << box.getLength() << " x " << box.getWidth() << " x " << box.getHeight()
         << " = " << box.getVolume()
         << endl;
}

// This is the wrong way to pass an object to a function
void displayBoxWithCopyConstructor(Box box) {
    cout << "length: " << box.getLength() << " x " << box.getWidth() << " x " << box.getHeight()
         << " = " << box.getVolume()
         << endl;
}

// This is also the wrong way to pass an object to a function
void displayBoxWithMutableReference(Box& box) {
    cout << "length: " << box.getLength() << " x " << box.getWidth() << " x " << box.getHeight()
         << " = " << box.getVolume()
         << endl;
}

int main() {
    // Call default constructor for stack variable
    Box box;
    displayBox(box);

    box.setLength(10);
    box.setWidth(20);
    box.setHeight(30);
    displayBox(box);

    // Call custom constructor for stack variable
    Box customBox(3, 9, 5);
    displayBox(customBox);

    // Call a constructor for a heap variable
    Box* pBox = new Box(10, 3, 12);
    displayBox(*pBox);
    delete pBox;

    /*
     * You can copy an object by using its copy constructor.  If you don't provide a copy constructor, then the compiler
     * will provide one for you.
     */
    Box copy(box);
    displayBox(copy);

    /*
     * You can also copy an object by using its overloaded assignment operator.  If you don't provide one, then the
     * compiler will provide one for you.
     */
    Box otherCopy = customBox;
    displayBox(otherCopy);

    const Box constBox(4, 9, 7);
    displayBox(constBox);
    // The following code won't compile because these member functions don't have the const attribute
//    constBox.setLength(10);
//    constBox.setWidth(9);
//    constBox.setHeight(15);

    /*
     * Because we use a stack variable as the parameter to this function, C++ can't pass by reference and has to create
     * a copy of the passed object.  This will always be slower than using a reference or pointer.
     */
    displayBoxWithCopyConstructor(box);

    /*
     * If your pass a value to a function by a reference, and that function does not mutate the referenced value, then
     * make sure that the parameter is declared as const.  Otherwise you will not be able to pass const objects to it.
     * Attempts to do so will result in a compiler error, like below.
     */
    //displayBoxWithMutableReference(constBox);

    return 0;
}