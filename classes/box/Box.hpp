// This file contains the class declaration, defining the interface that has to be used to access the class.

#ifndef TUTORIAL_BOX_HPP
#define TUTORIAL_BOX_HPP


class Box {
private:
    int length;
    int width;
    int height;

public:
    Box() : length(0), width(0), height(0) {}
    Box(int length, int width, int height) : length(length), width(width), height(height) {}

    // Member functions with the const attribute are the only member functions that can be called on a const instance
    int getLength() const { return length; }
    void setLength(int length) { this->length = length; }

    int getWidth() const { return width; }
    void setWidth(int width) { this->width = width; }

    int getHeight() const { return height; }
    void setHeight(int height) { this->height = height; }

    int getVolume() const { return length * width * height; }
};


#endif //TUTORIAL_BOX_HPP
