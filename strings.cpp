#include <iostream>
#include <cstring>
#include <cwchar>

// These are C functions ported to C++ for backwards compatibility.  Adding the using directive so you can see them
using std::strlen;
using std::strcpy;
using std::strcat;
using std::strcmp;

// These functions are from cwchar instead of cstring
using std::wcslen;
using std::wcscpy;
using std::wcscat;
using std::wcscmp;

void showAnatomyOfCStyleString() {
    // C-Style strings are null-terminated.
    // If you ever need to build one manually then always include the null character (\0)
    char buffer[] = { 'H', 'e', 'l', 'l', 'o', '\0' };
    std::cout << buffer << std::endl;

    // C-Style string literals are automatically null-terminated
    const char* otherBuffer = "Hello";
    std::cout << otherBuffer << std::endl;
}

void copyAndConcatenateCStyleStrings() {
    const char* hello = "hello";
    const char* world = "world";

    const size_t helloLength = std::strlen(hello);
    const size_t spaceLength = 1;
    const size_t worldLength = std::strlen(world);
    const size_t nullTerminatorLength = 1;

    const size_t helloWorldLength = helloLength + spaceLength + worldLength + nullTerminatorLength;
    char* helloWorld = new char[helloWorldLength];
    /* Always check that your buffer is big enough to hold the data.  C and C++ won't hold your hand like C# or Java
     * will.  If your buffer isn't big enough, you won't get an exception or error code.  You will get a buffer overflow,
     * which is a great way for attackers to modify your system.
     */
    std::strcpy(helloWorld, hello);
    helloWorld[helloLength] = ' ';
    // strcpy automatically adds a null-terminator, however we just overwrote it to add the space.
    helloWorld[helloLength + 1] = '\0';
    std::strcat(helloWorld, world);  // Also automatically adds a null-terminator
    std::cout << helloWorld << std::endl;
}

void compareCStyleStrings() {
    const char* hello = "hello";
    const char* world = "world";
    const char* foobar = "foo bar";

    std::cout << "compare hello to world (should be negative): " << strcmp(hello, world) << std::endl;
    std::cout << "compare hello to foobar (should be positive): " << strcmp(hello, foobar) << std::endl;
    std::cout << "compare hello to hello (should be zero): " << strcmp(hello, hello) << std::endl;
}

void showcaseStringClass() {
    // Can be automatically casted from a C-style string
    std::string hello = "hello";
    std::string foobar = "foobar";
    // Can also use a constructor
    std::string world("world");

    // Can concatenate them
    std::string helloWorld = hello + " " + world;
    std::cout << helloWorld << std::endl;

    // Can find the size
    std::cout << "size of hello world: " << helloWorld.size() << std::endl;

    // Can get a substring
    size_t startPos = 0;
    size_t length = 5;
    std::string substring = helloWorld.substr(startPos, length);
    std::cout << substring << std::endl;
    substring = helloWorld.substr(6); // gets from index six to the end of the string
    std::cout << substring << std::endl;

    // Can compare with any of the comparison operators
    if (hello < world) {
        std::cout << "hello < world" << std::endl;
    }

    // Find a character or string
    size_t positionOfFirstL = helloWorld.find('l');
    std::cout << "position of first l: " << positionOfFirstL << std::endl;

    size_t positionOfLo = helloWorld.find("lo");
    std::cout << "position of low: " << positionOfLo << std::endl;

    size_t positonOfLastL = helloWorld.rfind('l');
    std::cout << "position of last l: " << positonOfLastL << std::endl;

    // Find any character in the given string
    size_t positionOfFirstAssortedCharacter = helloWorld.find_first_of(" ;.");
    std::cout << "position of first space or semicolon or period: " << positionOfFirstAssortedCharacter << std::endl;

    size_t positonOfFirstNonAssortedCharacter = helloWorld.find_first_not_of("hijklmn");
    std::cout << "position of first character not in hijklmn: " << positonOfFirstNonAssortedCharacter << std::endl;

    // Cast a numeric value to a string
    std::string number = std::to_string(123);
    std::cout << number << std::endl;

    // Convert from a string
    int integer = std::stoi("23");
    long aLong = std::stol("9876543210");
    double aDouble = std::stod("987.654");

    std:: cout << integer << " " << aLong << " " << aDouble << std::endl;

    // Convert a string to a c-string
    const char* cString = hello.c_str();
    std::cout << "cString: " << cString << std::endl;
}

/**
 * C and C++ also support Unicode strings.  However, the encoding of Unicode is not standardized.  Most compilers use
 * UCS-4 encoding, which is four bytes long.  But some compilers use UCS-2, which is a subset of UTF-16 that is exactly
 * 2 bytes long.
 */
void showcaseUnicodeStrings() {
    const wchar_t* buffer = L"This is a unicode string";
    // Use wcout, wcerr, and wcin instead of cout, cerr and cin
    std::wcout << buffer << std::endl;

    size_t bufferSize = wcslen(buffer);
    std::cout << bufferSize << std::endl;

    std::wstring hello = L"hello";
    std::wstring world(L"world");
}

int main() {
    showAnatomyOfCStyleString();
    copyAndConcatenateCStyleStrings();
    compareCStyleStrings();
    showcaseStringClass();
    showcaseUnicodeStrings();
    return 0;
}