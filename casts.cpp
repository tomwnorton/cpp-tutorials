#include <cstdlib> // For the size_t type

struct Point {
    int x;
    int y;
};

int main() {
    long first = 23L;
    // C-style casts are discouraged, but still work in certain cases
    int second = (int) first;

    // The preferred way is to use static casts
    // NOTE: static casts only work when both the source and destination types are known at compile time.  If you need
    // to cast an Animal to a Dog, you will need a dynamic cast instead (more on that when we get to polymorphism).
    int third = static_cast<int>(first);

    // If you need to cast away const-ness on pointers then you need a const cast
    const int fourth = 98;
    const int* pointer = &fourth;
    int* otherPointer = const_cast<int*>(pointer);

    // The reinterpret cast is the most powerful and dangerous of all the casts.  Use it at your own peril.
    Point* point = new Point;
    size_t memoryAddress = reinterpret_cast<size_t>(point);
    delete point;

    return 0;
}