#include <iostream>

struct Box {
    int length;
    int width;
    int height;
};

int main() {
    int firstStackVariable = 12;
    int secondStackVariable = 30;

    // A reference can only be initialized to an existing values.
    int& firstReference = firstStackVariable;
    firstReference = 98;
    std::cout << "firstStackVariable: " << firstStackVariable << std::endl;

    // A reference CANNOT be reassigned.  All you can do is change the referenced value.
    firstReference = secondStackVariable;
    std::cout << "firstStackVariable: " << firstStackVariable << std::endl;

    // A reference to a union, struct or class uses the member access operator
    Box box;
    box.length = 10;
    box.width = 20;
    box.height = 30;

    Box& boxReference = box;
    boxReference.height = 15;
    std::cout << "box.height: " << box.height << std::endl;

    return 0;
}