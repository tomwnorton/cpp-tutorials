#include <iostream>

// std is a namespace
using std::cout;
using std::cin;
using std::endl;

void greet(const std::string& name) {
    cout << "Hello, " << name << "!" << endl;
}

namespace foo {
    void greet() {
        cout << "Hello!" << endl;
    }

    namespace bar {
        void greet() {
            cout << "Hello again!" << endl;
        }

        void greetFromCommandLine() {
            cout << "Enter Name> ";
            std::string name;
            std::getline(cin, name);
            ::greet(name);
        }
    }
}

// Continuation of foo namespace.  This showcases that a namespace can be modified from multiple sources
namespace foo {
    void doMath() {
        cout << " 1 + 1 = 2" << endl;
    }
}

/*
 * There is currently no short-hand for adding a namespace multiple levels deep.  This is supposed to change in the next
 * version of C++, slated to come out in 2017.
 */
namespace foo {
    namespace baz {
        void leave() {
            cout << "goodbye!";
        }
    }
}

/*
 * This namespace is only visible in this compilation unit
 */
namespace {
    void saySomething() {
        cout << "Hi!";
    }
}

namespace short_hand = foo::bar;

using namespace foo::baz;

int main() {
    foo::greet();
    foo::doMath();
    foo::bar::greet();
    short_hand::greetFromCommandLine();
    leave();
    saySomething();
    return 0;
}