#include <iostream>
#include <algorithm>
#include <string>

int main() {
    // Arrays can be static
    int staticArray[5];
    // The size of a static array is known at compile time
    std::cout << "number of bytes in staticArray: " << sizeof(staticArray) << std::endl;
    std::cout << "number of elements in staticArray: " << sizeof(staticArray) / sizeof(int) << std::endl;

    // Arrays can be initialized too:
    int otherStaticArray[] = {10, 30, 20, 16};
    size_t otherStaticArrayLength = sizeof(otherStaticArray) / sizeof(int);
    std::cout << "number of elements in otherStaticArray: " << otherStaticArrayLength << std::endl;

    // Loop through them with a for loop
    for (int i = 0; i < otherStaticArrayLength; ++i) {
        std::cout << otherStaticArray[i] << " ";
    }
    std::cout << std::endl;

    // Loop through them with a ranged-for loop
    for (int element : otherStaticArray) {
        std::cout << element << " ";
    }
    std::cout << std::endl;

    // In C and C++, array syntax is really just syntactic sugar for pointers
    int* pointer = otherStaticArray;
    for (int i = 0; i < otherStaticArrayLength; ++i) {
        std::cout << pointer[i] << " ";
    }
    std::cout << std::endl;

    // You can also use pointer arithmetic instead of the array syntax
    for (int i = 0; i < otherStaticArrayLength; ++i) {
        std::cout << *(pointer + i) << " ";
    }
    std::cout << std::endl;

    // You can also treat a pointer as an iterator using pointer arithmetic
    int* end = otherStaticArray + otherStaticArrayLength;
    for (int* pos = otherStaticArray; pos != end; ++pos) {
        std::cout << *pos << " ";
    }


    // You can also create dynamic arrays
    int* dynamicArray = new int[5];

    /* C and C++ don't store array length information.  They know about the size of static arrays because those arrays
     * are initialized at compile time.  Because dynamic arrays are initialized at runtime the programmer has to manage
     * the array.  You can do this by storing the size of the array in a variable or by having a terminating element.
     * Both are common practice in C and C++.  If you need to pass the array to different functions, then you have
     * to use a terminating element.  The most common terminating element is NULL, which is the same as 0 in C and C++
     */
    for (int i = 0; i < 4; ++i) {
        dynamicArray[i] = i * i;
    }
    dynamicArray[4] = NULL;

    // The ranged for only works on dynamic arrays.
    for (int* pos = dynamicArray; *pos != 0; ++pos) {
        std::cout << *pos << " ";
    }

    // Don't forget to delete the memory you allocate from the heap.  Notice the difference in syntax between deleting
    // a pointer to a single element and deleting an array
    delete[] dynamicArray;

    // Sum all numbers in the array
    int numbersToSum[] = {9, 8, 5, 22, 33};
    int numbersToSumLength = sizeof(numbersToSum) / sizeof(int);
    int sum = std::accumulate(numbersToSum, numbersToSum + numbersToSumLength, 0);
    std::cout << "Sum of all numbers: " << sum << std::endl;

    // Sum the three middle numbers in the array
    sum = std::accumulate(numbersToSum + 1, numbersToSum + 4, 0);
    std::cout << "Sum of three middle numbers: " << sum << std::endl;

    // Concatenate all strings in an array
    // auto is used to declare a variable using type inference.  In other words it is equivalent of var in C#
    std::string strings[] = {"hello", "world", "foo", "bar"};
    int stringsLength = sizeof(strings) / sizeof(std::string);
    std::string fullString = std::accumulate(strings, strings + stringsLength, (std::string) "",
                                             [](auto lhs, auto rhs) {
                                                 return lhs + " " + rhs;
                                             });
    std::cout << "Full String: " << fullString << std::endl;

    // Get a pointer to the first occurrence of 22 in the array
    int* pos = std::find(numbersToSum, numbersToSum + numbersToSumLength, 22);
    // If it is not found, then pos points to the first position after the array
    if (pos != numbersToSum + numbersToSumLength) {
        std::cout << "First occurrence of " << *pos << " found at index " << pos - numbersToSum << std::endl;
    }

    // Get a pointer to the first element greater than 5
    int* greaterThanPos = std::find_if(numbersToSum, numbersToSum + numbersToSumLength, [](auto element) {
        return element > 5;
    });
    if (pos != numbersToSum + numbersToSumLength) {
        std::cout << "First element greater less than five is " << *greaterThanPos
                  << " found at index " << greaterThanPos - numbersToSum << std::endl;
    }

    // Get a copy of all elements
    int copyOfAllNumbers[6];
    int* positionAfterCopy = std::copy(numbersToSum, numbersToSum + numbersToSumLength, copyOfAllNumbers);
    *positionAfterCopy = -93;
    for (int number : copyOfAllNumbers) {
        std::cout << number << " ";
    }
    std::cout << std::endl;

    // Get a copy of some elements
    int copyOfSomeElements[10];
    positionAfterCopy = std::copy_if(numbersToSum, numbersToSum + numbersToSumLength, copyOfSomeElements,
                                     [](auto element) { return element % 2 != 0; });
    for (pos = copyOfSomeElements; pos != positionAfterCopy; ++pos) {
        std::cout << *pos << " ";
    }
    std::cout << std::endl;

    return 0;
}